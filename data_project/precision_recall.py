#!/usr/bin/python
# -*- coding: utf-8 -*-

import io

def evaluate(relevant_doc_list, answer_set_list):
    precision_recall = []
    relevant_len = len(relevant_doc_list)
    recall_lvl = 0.0
    answers = 0.0
    for i in answer_set_list:
        answers += 1
        if i in relevant_doc_list:
            recall_lvl += 1
            recall = recall_lvl * 100 / relevant_len
            precision = recall_lvl * 100 / answers
            precision_recall.append([recall, precision])
    return precision_recall


relevant0 = [1155, 6673, 4651, 8959, 6424, 5514, 8303, 1214, 4789, 1734]
relevant1 = [29, 30, 28, 31, 27, 32, 26, 33, 34, 25]
relevant2 = [29, 30, 28, 31, 27, 32, 26, 33, 34, 25]

d0_all = [9887, 1039, 6109, 303, 4149, 928, 4596, 1615, 3860, 4563]

d1_10 = [30, 31, 29, 25, 24, 34, 28, 42, 32, 21]
d1_15 = [30, 31, 29, 34, 28, 25, 24, 26, 32, 27]
d1_50 = [30, 31, 29, 34, 28, 33, 25, 26, 24, 32]

d2_10 = [52, 53, 51, 50, 49, 54, 48, 47, 46, 57]
d2_15 = [52, 51, 50, 49, 48, 47, 53, 46, 45, 44]
d2_50 = [51, 50, 49, 48, 47, 46, 45, 44, 43, 42]


pr_d0_all = []
pr_d1_10 = []
pr_d1_15 = []
pr_d1_50 = []
pr_d2_10 = []
pr_d2_15 = []
pr_d2_50 = []
pr_d1_count = []

pr_d0_all = evaluate(relevant0, d0_all)

pr_d1_10 =  evaluate(relevant1, d1_10)
pr_d1_15 =  evaluate(relevant1, d1_15)
pr_d1_50 =  evaluate(relevant1, d1_50)
pr_d1_count =  evaluate(relevant1, relevant1)

pr_d2_10 =  evaluate(relevant2, d2_10)
pr_d2_15 =  evaluate(relevant2, d2_15)
pr_d2_50 =  evaluate(relevant2, d2_50)

with io.open('results.m', 'w') as f:
    f.write(unicode('r_count = ['))
    for recall, precision in pr_d1_count:
        f.write(unicode(str(recall) + ' '))
    f.write(unicode('];\np_count = ['))
    for recall, precision in pr_d1_count:
        f.write(unicode(str(precision) + ' '))
    f.write(unicode('];\n\n'))
    
    f.write(unicode('r10 = ['))
    for recall, precision in pr_d1_10:
        f.write(unicode(str(recall) + ' '))
    f.write(unicode('];\np10 = ['))
    for recall, precision in pr_d1_10:
        f.write(unicode(str(precision) + ' '))
    f.write(unicode('];\n'))
    
    f.write(unicode('r15 = ['))
    for recall, precision in pr_d1_15:
        f.write(unicode(str(recall) + ' '))
    f.write(unicode('];\np15 = ['))
    for recall, precision in pr_d1_15:
        f.write(unicode(str(precision) + ' '))
    f.write(unicode('];\n'))
    
    f.write(unicode('r50 = ['))
    for recall, precision in pr_d1_50:
        f.write(unicode(str(recall) + ' '))
    f.write(unicode('];\np50 = ['))
    for recall, precision in pr_d1_50:
        f.write(unicode(str(precision) + ' '))
    f.write(unicode('];\n\n'))
    
    f.write(unicode('plot(r_count, p_count, r10, p10, r15, p15, r50, p50);\n'))
    f.write(unicode('grid on;\n'))
    f.write(unicode('axis([10 100 70 103]);\n'))
    f.write(unicode("xlabel('Recall')\n"))
    f.write(unicode("ylabel('Precision')\n"))
    f.write(unicode("title('Precision-Recall for dataset\_2');\n"))
    f.write(unicode("legend('Location','Best','count', 'approx\_count(10)','approx\_count(15)','approx\_count(50)');\n"))
    